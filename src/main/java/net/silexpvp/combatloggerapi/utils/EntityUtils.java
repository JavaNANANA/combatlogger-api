package net.silexpvp.combatloggerapi.utils;

import java.lang.reflect.Field;

public class EntityUtils {
    public static Object getPrivateField(String fieldName, Class clazz, Object object) {
        Field field;
        Object objectEqual = null;

        try {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            objectEqual = field.get(object);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return objectEqual;
    }
}
