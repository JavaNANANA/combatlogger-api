package net.silexpvp.combatloggerapi.entity.enums;

import net.minecraft.server.v1_8_R3.Entity;
import net.silexpvp.combatloggerapi.entity.CombatLogger;

import java.util.Map;

import static net.silexpvp.combatloggerapi.utils.EntityUtils.getPrivateField;

public enum EntityTypes {
    CUSTOM_ZOMBIE("Zombie", 54, CombatLogger.class);

    EntityTypes(String name, int id, Class<? extends Entity> custom) {
        addToMaps(custom, name, id);
    }

    private static void addToMaps(Class clazz, String name, int id) {
        ((Map) getPrivateField("c", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(name, clazz);
        ((Map) getPrivateField("d", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(clazz, name);
        ((Map) getPrivateField("f", net.minecraft.server.v1_8_R3.EntityTypes.class, null)).put(clazz, id);
    }
}