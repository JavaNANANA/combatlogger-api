package net.silexpvp.combatloggerapi.entity;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.EntityZombie;
import net.minecraft.server.v1_8_R3.PathfinderGoalSelector;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import java.util.List;
import java.util.UUID;

import static net.silexpvp.combatloggerapi.utils.EntityUtils.getPrivateField;

@Getter
public class CombatLogger extends EntityZombie {
    private UUID uniqueId;

    public CombatLogger(UUID uniqueId, Location location) {
        super(((CraftWorld) location.getWorld()).getHandle());

        this.uniqueId = uniqueId;

        List goalB = (List) getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
        goalB.clear();

        List goalC = (List) getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();

        List targetB = (List) getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
        targetB.clear();

        List targetC = (List) getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        targetC.clear();

        this.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        ((CraftWorld) location.getWorld()).getHandle().addEntity(this);
    }
}
