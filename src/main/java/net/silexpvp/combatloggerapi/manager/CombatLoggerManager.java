package net.silexpvp.combatloggerapi.manager;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.EntityZombie;
import net.silexpvp.combatloggerapi.entity.CombatLogger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
public class CombatLoggerManager {
    private Map<String, CombatLogger> combatLoggerMap = new HashMap<>();

    public void createCombatEntity(Player player) {
        LivingEntity entity = (LivingEntity) new CombatLogger(player.getUniqueId(), player.getLocation());

        entity.setRemoveWhenFarAway(false);
        entity.setMaxHealth(player.getMaxHealth() * 2);
        entity.setHealth(player.getHealth() * 2);
        entity.setFallDistance(player.getFallDistance());
        entity.setLastDamageCause(player.getLastDamageCause());

        EntityZombie combatLogger = (EntityZombie) entity;

        combatLogger.setBaby(false);
        combatLogger.setInvisible(false);
        combatLogger.setCustomName(ChatColor.RED + player.getName());
        combatLogger.setCustomNameVisible(true);
        combatLogger.setVillager(false);
    }

    public void removeCombatEntity(String name) {
        CombatLogger combatLogger = combatLoggerMap.get(name);

        combatLogger.getBukkitEntity().remove();
        combatLoggerMap.remove(name);
    }

    public void removeCombatEntity(UUID uniqueId) {
        String name = Bukkit.getOfflinePlayer(uniqueId).getName();

        removeCombatEntity(name);
    }

    public void removeCombatEntity(Player player) {
        removeCombatEntity(player.getName());
    }


    public CombatLogger getByEntity(LivingEntity entity) {
        for (CombatLogger logger : combatLoggerMap.values()) {
            if (logger.getBukkitEntity().equals(entity)) {
                return logger;
            }
        }
        return null;
    }

    public CombatLogger getByPlayer(Player player) {
        for (CombatLogger logger : combatLoggerMap.values()) {
            if (logger.getUniqueId().equals(player.getUniqueId())) {
                return logger;
            }
        }
        return null;
    }

    public CombatLogger getByName(String name) {
        for (CombatLogger logger : combatLoggerMap.values()) {
            if (logger.getName().equalsIgnoreCase(name)) {
                return logger;
            }
        }
        return null;
    }
}
