package net.silexpvp.combatloggerapi;

import lombok.Getter;
import net.silexpvp.combatloggerapi.listeners.CombatLoggerListener;
import net.silexpvp.combatloggerapi.manager.CombatLoggerManager;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class CombatLoggerAPI {
    @Getter private static JavaPlugin plugin;

    private CombatLoggerManager combatLoggerManager;

    public void setEnvironment(JavaPlugin plugin) {
        this.plugin = plugin;

        combatLoggerManager = new CombatLoggerManager();

        plugin.getServer().getPluginManager().registerEvents(new CombatLoggerListener(), plugin);
    }
}
